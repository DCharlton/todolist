#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

struct todo{
  int id;
  int level;
  string name;
  string description;
  bool deleted = false;
};

// File
bool saveToFile();
bool loadFromFile();

// To-Do
bool addtodo();
bool removetodo();
bool modifytodo();
void printtodos();

// Utility
void printString(string out);
void clearScreen();

// Variables
char action[10];
todo *todos[1000];
int todoTop = 0;

int countIn = 0;
int levelIn[5] = {};

int main()
{
  // Print Info
  // cout << string(48, '-') << endl;
  if (!loadFromFile()) printString("Failed to read from file.");
  else{
    // printString(to_string(countIn) + " To-Do's");
    // printString(to_string(levelIn[0]) + " Level 0");
    // printString(to_string(levelIn[1]) + " Level 1");
    // printString(to_string(levelIn[2]) + " Level 2");
    // printString(to_string(levelIn[3]) + " Level 3");
    // printString(to_string(levelIn[4]) + " Level 4");
    // printString(to_string(levelIn[5]) + " Level 5");
  }
  // cout << string(48, '-') << endl;

  // Main Loop
  while (true)
  {
    cout << endl << string(48, '-') << endl;

    printtodos();

    cout << string(48, '-') << endl << endl;

    cout << "(q) quit, (n) new, (d) delete, (m) modify:\n> ";
    cin >> action;
    cout << endl;

    if (action[0] == 'q'){
      if (!saveToFile())
        cout << "Failed to write to file." << endl;
      else
        cout << "List saved successfully" << endl;
      
      break;
    }
    else if (action[0] == 'n'){
      if(!addtodo())
        cout << "Failed to add todo" << endl;
    }
    else if (action[0] == 'd'){
      if (!removetodo())
        cout << "Failed to remove todo" << endl;
    }
    else if (action[0] == 'm'){
      if (!modifytodo())
        cout << "Failed to modify todo" << endl; 
    }
    // clearScreen();
  }
  // cout << "--------------------------------" << endl;

  return 0;
}

void printtodos()
{
  cout << endl;
  for (int i = 0; i < todoTop; i++)
  {
    if (!todos[i]->deleted)
    {
      cout << setw(3) << left << "ID" << setw(10) << left << "Name" << endl;// << setw(6) << left << "Level" << endl;
      cout << setw(3) << left << todos[i]->id << setw(10) << left << todos[i]->name << endl;// << setw(6) << left << todos[i]->level << endl;
      cout << todos[i]->description << endl << endl;
    }
  }
}

bool modifytodo()
{
  cout << endl;
  
  int id;
  cout << "ToDo ID To Modify:\n> ";
  cin >> id;

  cout << "(n) for name, (d) for description, (l) for level:\n> ";
}

void clearScreen()
{
  cout << string(100, '\n');
}

bool addtodo()
{
  cout << endl;
  char name[100], desc[1000];
  int level;

  cout << "Enter Task Name:\n> ";
  cin.ignore();
  cin.getline(name, 100);

  cout << "Enter Task Priority:\n> ";
  cin >> level;

  cout << "Enter Task Description:\n> ";
  cin.ignore();
  cin.getline(desc, 1000);

  todo *temp = new todo();
  temp->id = todoTop;
  temp->name = name;
  temp->level = level;
  temp->description = desc;

  todos[todoTop] = temp;
  todoTop++;

  return true;
}

bool removetodo()
{
  int id;
  cout << "ToDo ID To Remove:\n> ";
  cin >> id;

  todos[id]->deleted = true;
  return true;
}

void printString(string out)
{
  cout << "| " << setw(44) << left << out << " |" << endl;
}

bool saveToFile()
{
  ofstream fout;
  fout.open("todo.txt");

  for (int i = 0; i < todoTop; i++)
  {
    if (!todos[i]->deleted){
      fout << todos[i]->name << endl;
      fout <<  todos[i]->level << endl;
      fout <<  todos[i]->description << endl;
    }
  }

  fout.close();

  return true;
}

bool loadFromFile()
{
  string n, d;
  int l;

  ifstream fin;
  fin.open("todo.txt");

  while (getline(fin, n) && fin >> l && fin.ignore() && getline(fin, d))
  {
    countIn++;
    levelIn[l]++;

    todo* temp = new todo();
    temp->id = todoTop;
    temp->level = l;
    temp->name = n;
    temp->description = d;
    
    todos[todoTop] = temp;
    todoTop++;
  }

  fin.close();
  return true;
}