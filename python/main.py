from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout

def WidgetScreen():
    layout = BoxLayout()
    btn = Button(text='Hello World')
    layout.add_widget(btn)
    return layout

class MyApp(App):
    def build(self):
        return WidgetScreen()

if __name__=='__main__':
    MyApp().run()

# import gi
# gi.require_version("Gtk", "3.0")
# from gi.repository import Gtk

# class MyWindow(Gtk.Window):

#     def __init__(self):
#         Gtk.Window.__init__(self, title="Hello World")
#         # Gtk.Window.set_size_request(600, 400)
#         self.button = Gtk.Button(label="Click Here")
#         self.button.connect("clicked", self.on_button_clicked)
#         self.add(self.button)

#     def on_button_clicked(self, widget):
#         print("Hello World")

# win = MyWindow()
# win.connect("destroy", Gtk.main_quit)
# win.show_all()
# Gtk.main()
